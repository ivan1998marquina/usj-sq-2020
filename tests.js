test("True", function (assert) {
    assert.equal(isValidPlate("1111FFF"), true, "SAME NUMBERS, SAME LETTERS");
    assert.equal(isValidPlate("1234FFF"), true, "DIFERENT NUMBERS,SAME LETTERS");
    assert.equal(isValidPlate("1111RTY"), true, "SAME NUMBERS, DIFERENT LETTERS");
    assert.equal(isValidPlate("1234RTY"), true, "DIFERENT NUMBERS, DIFERENT LETTERS");
    assert.equal(isValidPlate("1111fFf"), true, "LOWERCASE AND UPPERCASE");

});

test("Size failure", function (assert) {
    assert.equal(isValidPlate("111FFF"), false, "lESS THAT 4 NUMBERS");
    assert.equal(isValidPlate("1111FF"), false, "LESS THAT 3 LETTERS");
    assert.equal(isValidPlate("111FF"), false, "LESS OF BOTH");
    assert.equal(isValidPlate(""), false, "EMPTY STRING");
});

test("Invalid parameters", function (assert) {
    assert.equal(isValidPlate("1G1GFFF"), false, "LETTERS IN THE NUMBERS");
    assert.equal(isValidPlate("1111F2F"), false, "NUMBERS IN THE LETTERS");
    assert.equal(isValidPlate("1F1F1F1"), false, "NUMBERS AND LETTERS IN BOTH SIDE");
    assert.equal(isValidPlate("1111AAA"), false, "VOWELS IN THE LETERS");
    assert.equal(isValidPlate("1111QQQ"), false, "Q IN THE LETTERS");
    assert.equal(isValidPlate("1111ÑÑÑ"), false, "Ñ IN THE LETTERS");
});

test("SPECIAL CHARACTERS", function (assert) {
    assert.equal(isValidPlate("1()1FFF"), false, "SPECIAL CHARACTERS IN LETTERS");
    assert.equal(isValidPlate("1111F()"), false, "SPECIAL CHARACTERS IN NUMBERS");
    assert.equal(isValidPlate("1111/FFF"), false, "SPECIAL CHARACTERS IN THE MIDDLE");
    assert.equal(isValidPlate("()11F()"), false, "SPECIAL CHARACTERS IN BOTH");
    assert.equal(isValidPlate("1111 FFF"), false, "EMPTY SPACE IN THE MIDDLE");
    assert.equal(isValidPlate("11 11FFF"), false, "EMPTY SPACE IN THE NUMBERS");
    assert.equal(isValidPlate("1111FF F"), false, "EMPTY SPACE IN THE LETTERS");
});



